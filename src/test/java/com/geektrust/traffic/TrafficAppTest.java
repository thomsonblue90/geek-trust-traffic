package com.geektrust.traffic;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.io.PrintStream;

public class TrafficAppTest {

    @Mock
    PrintStream printStream;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        System.setOut(printStream);
    }

    @Test
    public void mainTest() throws IOException {
        TrafficApp.main(new String[]{"src/main/resources/sample.txt"});
        Mockito.verify(printStream, Mockito.times(2)).println("CAR ORBIT2");
        Mockito.verify(printStream, Mockito.times(1)).println("TUKTUK ORBIT1");
        Mockito.verify(printStream, Mockito.times(1)).println("No suitable journey found");
        Mockito.verify(printStream, Mockito.times(1)).println("Traffic Speeds don't match with available orbits");
        Mockito.verify(printStream, Mockito.times(1)).println("TUKTUK ORBIT2");
    }

}
