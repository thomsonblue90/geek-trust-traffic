package com.geektrust.traffic.exception;

public class ArgumentsMisMatchException extends Exception {
    public ArgumentsMisMatchException(String message) {
        super(message);
    }
}
