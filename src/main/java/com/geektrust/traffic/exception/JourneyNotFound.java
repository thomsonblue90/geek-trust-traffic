package com.geektrust.traffic.exception;

public class JourneyNotFound extends Exception {
    public JourneyNotFound(String message) {
        super(message);
    }
}
