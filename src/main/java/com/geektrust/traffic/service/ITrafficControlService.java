package com.geektrust.traffic.service;

import com.geektrust.traffic.domain.JourneyPlan;
import com.geektrust.traffic.exception.ArgumentsMisMatchException;
import com.geektrust.traffic.exception.JourneyNotFound;
import com.geektrust.traffic.util.Constants;

import java.util.List;

public interface ITrafficControlService extends Constants {
    JourneyPlan planJourney(String weatherName, List<Float> trafficSpeeds)
            throws JourneyNotFound, ArgumentsMisMatchException;
}
