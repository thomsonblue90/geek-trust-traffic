package com.geektrust.traffic.service;

import com.geektrust.traffic.domain.JourneyPlan;
import com.geektrust.traffic.domain.Orbit;
import com.geektrust.traffic.domain.Vehicle;
import com.geektrust.traffic.domain.Weather;
import com.geektrust.traffic.exception.ArgumentsMisMatchException;
import com.geektrust.traffic.exception.JourneyNotFound;
import com.geektrust.traffic.util.Calculator;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.geektrust.traffic.domain.Weather.*;

public class TrafficControlService implements ITrafficControlService {

    private List<Vehicle> vehicles = new ArrayList<>();
    private List<Orbit> orbits = new ArrayList<>();

    public TrafficControlService() {
        initializeOrbits();
        initializeVehicles();
    }

    public JourneyPlan planJourney(String weatherName, List<Float> trafficSpeeds)
            throws JourneyNotFound, ArgumentsMisMatchException {
        Weather weather = Weather.valueOf(weatherName.toUpperCase());
        List<Orbit> orbitsWithTraffic = getOrbitsWithTrafficSpeed(trafficSpeeds);
        List<JourneyPlan> journeyPlans = new ArrayList<>();
        for (Orbit orbit : orbitsWithTraffic) {
            for (Vehicle vehicle : vehicles) {
                journeyPlans.add(getJourneyPlan(weather, orbit, vehicle));
            }
        }
        Collections.sort(journeyPlans);
        JourneyPlan fastestJourneyPlan = journeyPlans.get(0);
        if (fastestJourneyPlan.getTotalTime() == Float.MAX_VALUE) {
            throw new JourneyNotFound("No suitable journey found");
        }
        return fastestJourneyPlan;
    }

    private JourneyPlan getJourneyPlan(Weather weather, Orbit orbit, Vehicle vehicle) {
        JourneyPlan journeyPlan = new JourneyPlan(vehicle.getName(), vehicle.getPreference(), orbit.getName());
        if (vehicle.isWeatherSafe(weather)) {
            float maxSpeed = Math.min(vehicle.getMaxSpeed(), orbit.getTrafficSpeed());
            int effectiveCraters = weather.getReducedCraters(orbit.getCraters());
            journeyPlan.setTotalTime(Calculator.
                    timeToTravel(orbit.getDistance(), maxSpeed, vehicle.getTimeToCrossCrater(), effectiveCraters));
        }
        return journeyPlan;
    }

    private List<Orbit> getOrbitsWithTrafficSpeed(List<Float> trafficSpeeds) throws ArgumentsMisMatchException {
        if (trafficSpeeds.size() != orbits.size()) {
            throw new ArgumentsMisMatchException("Traffic Speeds don't match with available orbits");
        }
        List<Orbit> orbitsWithTrafficSpeed = new ArrayList<>();
        for (int i = 0; i < orbits.size(); i++) {
            Orbit orbit = orbits.get(i);
            float trafficSpeed = trafficSpeeds.get(i);
            orbitsWithTrafficSpeed.add(new Orbit(orbit.getName(), orbit.getDistance(), orbit.getCraters(), trafficSpeed));
        }
        return orbitsWithTrafficSpeed;
    }

    private void initializeOrbits() {
        addOrbit(ORBIT1, 18f, 20);
        addOrbit(ORBIT2, 20f, 10);
    }

    private void initializeVehicles() {
        addVehicle(BIKE, 3, 10f, 2f, Arrays.asList(SUNNY, WINDY));
        addVehicle(TUKTUK, 2, 12f, 1f, Arrays.asList(SUNNY, RAINY));
        addVehicle(SUPERCAR, 1,20f,3f, Arrays.asList(SUNNY, RAINY, WINDY));
    }

    private void addVehicle(String name, Integer preference, float maxSpeed, float timeToCrossCrater, List<Weather> weathers) {
        vehicles.add(new Vehicle(name, preference, maxSpeed, timeToCrossCrater, weathers));
    }

    private void addOrbit(String name, float distance, Integer craters) {
        orbits.add(new Orbit(name, distance, craters));
    }

}
