package com.geektrust.traffic.domain;

import java.util.List;

public class Vehicle {

    private String name;
    private Integer preference;
    private Float maxSpeed;
    private Float timeToCrossCrater;
    private List<Weather> safeWeathers;

    public Vehicle(String name, Integer preference, Float maxSpeed, Float timeToCrossCrater, List<Weather> weathers) {
        this.name = name;
        this.maxSpeed = maxSpeed;
        this.timeToCrossCrater = timeToCrossCrater;
        this.safeWeathers = weathers;
        this.preference = preference;
    }

    public boolean isWeatherSafe(Weather weather) {
        return this.safeWeathers.contains(weather);
    }

    public String getName() {
        return name;
    }

    public Float getMaxSpeed() {
        return maxSpeed;
    }

    public Float getTimeToCrossCrater() {
        return timeToCrossCrater;
    }

    public Integer getPreference() {
        return preference;
    }
}
