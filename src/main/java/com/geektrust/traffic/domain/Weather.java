package com.geektrust.traffic.domain;

import com.geektrust.traffic.util.Calculator;

public enum Weather {
    SUNNY(-10),
    RAINY(20),
    WINDY(0);

    private float reducedCraterFactor;

    Weather(float reducedCraterFactor) {
        this.reducedCraterFactor = reducedCraterFactor;
    }

    public int getReducedCraters(int originalCraters) {
        int reducedCraters = Calculator.reducedCraters(originalCraters, this.reducedCraterFactor);
        return reducedCraters;
    }
}
