package com.geektrust.traffic.domain;

public class Orbit {

    private String name;
    private Float distance;
    private Integer craters;
    private Float trafficSpeed;

    public Orbit (String name, Float distance, Integer craters) {
        this.name = name;
        this.distance = distance;
        this.craters = craters;
    }

    public Orbit (String name, Float distance, Integer craters, Float trafficSpeed) {
        this.name = name;
        this.distance = distance;
        this.craters = craters;
        this.trafficSpeed = trafficSpeed;
    }

    public String getName() {
        return name;
    }

    public Float getDistance() {
        return distance;
    }

    public Integer getCraters() {
        return craters;
    }

    public Float getTrafficSpeed() {
        return trafficSpeed;
    }

    public void setTrafficSpeed(Float trafficSpeed) {
        this.trafficSpeed = trafficSpeed;
    }
}
