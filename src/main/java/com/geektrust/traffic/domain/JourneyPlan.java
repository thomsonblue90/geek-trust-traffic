package com.geektrust.traffic.domain;

import com.geektrust.traffic.util.Constants;

public class JourneyPlan implements Comparable<JourneyPlan>, Constants {

    private String vehicleName;
    private Integer vehiclePreference;
    private String orbitName;
    private Float totalTime = Float.MAX_VALUE;

    public JourneyPlan(String vehicleName, Integer vehiclePreference, String orbitName) {
        this.vehicleName = vehicleName;
        this.vehiclePreference = vehiclePreference;
        this.orbitName = orbitName;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public String getOrbitName() {
        return orbitName;
    }

    public Float getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Float totalTime) {
        this.totalTime = totalTime;
    }

    @Override
    public int compareTo(JourneyPlan journeyPlan) {
        int diff = (int)(totalTime - journeyPlan.totalTime);
        if (diff == 0) {
            return vehiclePreference - journeyPlan.vehiclePreference;
        }
        return diff;
    }
}
