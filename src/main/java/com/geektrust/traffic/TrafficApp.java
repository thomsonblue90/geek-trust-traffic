package com.geektrust.traffic;

import com.geektrust.traffic.domain.JourneyPlan;
import com.geektrust.traffic.exception.ArgumentsMisMatchException;
import com.geektrust.traffic.exception.JourneyNotFound;
import com.geektrust.traffic.service.ITrafficControlService;
import com.geektrust.traffic.service.TrafficControlService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TrafficApp {

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(args[0]));
        String value = bufferedReader.readLine();
        while (value != null) {
            List<String> inputs = Arrays.stream(value.split(" ")).collect(Collectors.toList());
            String weather = inputs.remove(0);
            List<Float> trafficSpeeds = inputs.stream().map(Float::new).collect(Collectors.toList());
            findFastestJourney(weather, trafficSpeeds);
            value = bufferedReader.readLine();
        }
    }

    private static void findFastestJourney(String weather, List<Float> trafficSpeeds) {
        ITrafficControlService trafficControlService = new TrafficControlService();
        try {
            JourneyPlan journeyPlan = trafficControlService.planJourney(weather, trafficSpeeds);
            System.out.println(journeyPlan.getVehicleName() + " " + journeyPlan.getOrbitName());
        } catch (JourneyNotFound | ArgumentsMisMatchException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
