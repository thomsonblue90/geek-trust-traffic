package com.geektrust.traffic.util;

public class Calculator {

    public static float timeToTravel(float distance, float speed, float craterSpeed, int crater) {
        return timeToTravel(distance, speed) + timeToTravel(craterSpeed, crater);
    }

    public static float timeToTravel(float distance, float speed) {
        return (distance / speed) * 60;
    }

    public static float timeToTravel(float craterSpeed, int crater) {
        return craterSpeed * crater;
    }

    public static int reducedCraters(int originalCraters, float reductionFactor) {
        return (int) (((100 + reductionFactor) / 100) * originalCraters);
    }
}
